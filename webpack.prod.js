const TerserPlugin = require("terser-webpack-plugin");
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const {merge} = require('webpack-merge');
const common = require('./webpack.common.js');

const production = {
	mode: 'production',
	optimization: {
		minimize: true,
		minimizer: [
			new CssMinimizerPlugin(
				{
					parallel: 4,
					minimizerOptions: {
						preset: [
							"default",
							{
								discardComments: {removeAll: true},
							},
						],
					}
				}
			),
			new TerserPlugin(
				{
					test: /\.js(\?.*)?$/i,
					parallel: true,
					include: undefined,
					exclude: undefined,
					minify: (file, sourceMap) => {
						// https://github.com/mishoo/UglifyJS2#minify-options
						const uglifyJsOptions = {
							/* your `uglify-js` package options */
						};

						if (sourceMap) {
							uglifyJsOptions.sourceMap = {
								content: sourceMap,
							};
						}

						return require("uglify-js").minify(file, uglifyJsOptions);
					},
					terserOptions: {
						format: {
							comments: false,
						},
						compress: {
							arrows: false,
							collapse_vars: false,
							comparisons: false,
							computed_props: false,
							hoist_funs: false,
							hoist_props: false,
							hoist_vars: false,
							inline: false,
							loops: false,
							negate_iife: false,
							properties: false,
							reduce_funcs: false,
							reduce_vars: false,
							switches: false,
							toplevel: false,
							typeofs: false,
							booleans: true,
							if_return: true,
							sequences: true,
							unused: true,
							conditionals: true,
							dead_code: true,
							evaluate: true
						},
						mangle: {
							safari10: true
						}
					}
				}
			)

		]
	},

};

module.exports = merge(common, production)