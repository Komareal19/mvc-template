const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const path = require("path");
const devMode = process.env.NODE_ENV !== "production";
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const webpack = require("webpack");

const url = 'extranet';


module.exports = {
	stats: devMode ? 'verbose' : 'minimal',
	entry: [
		'/App/Assets/js/app.js'
	],
	output: {
		path: path.resolve(__dirname, 'www/dist'),
		publicPath: '/' + url + '/www/dist/',
		filename: 'js/[name].[contenthash].js',
		chunkFilename: 'js/[name].js',
		clean: true
	},
	plugins: [
		new MiniCssExtractPlugin({
			filename: "styles/[name].[contenthash].css",
			chunkFilename: "styles/[id].[contenthash].css",
		}),
		new CleanWebpackPlugin(),
		new webpack.ProvidePlugin({
			$: 'jquery',
			jQuery: 'jquery',
			Util: 'exports-loader?Util!bootstrap/js/dist/util'
		}),
	],
	module: {
		rules: [
			{
				test: /\.scss$/,
				exclude: /node_modules/,
				use: [
					{
						loader: MiniCssExtractPlugin.loader,
					},
					{
						// translates CSS into CommonJS modules
						loader: 'css-loader'
					}, {
						// Run postcss actions
						loader: 'postcss-loader',
						options: {
							// `postcssOptions` is needed for postcss 8.x;
							// if you use postcss 7.x skip the key
							postcssOptions: {
								// postcss plugins, can be exported to postcss.config.js
								plugins: function () {
									return [
										require('autoprefixer')
									];
								}
							}
						}
					}, {
						// compiles Sass to CSS
						loader: 'sass-loader'
					}
				]

			},
			{
				test: /\.(png|jpg|gif)$/,
				exclude: /node_modules/,
				use: [
					{
						loader: 'file-loader',
						options: {
							name: '[folder]/[name].[ext]',
							outputPath: 'images',
							publicPath: '/' + url + '/www/dist/images'
						}
					}
				]
			},
			{
				test: /\.svg$/,
				use: [
					{
						loader: 'svg-url-loader',
						options: {
							limit: 10000,
							name: '[name].[ext]',
							outputPath: 'icons',
							publicPath: '/' + url + '/www/dist/icons'
						}
					}
				]
			}
		]
	},
	performance: {
		maxEntrypointSize: 1024000,
		maxAssetSize: 1024000
	},

};