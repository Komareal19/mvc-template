<?php

namespace Core;

use Model\FileReader;

/*
 *
 * 	Main Routing class
 *
 * 	First class that loads. It loads routes from the Routes.json and Components.json
 * 	(and pre-prepared DefaultComponents.json and merges it with user-made file)
 * 	then it renders main page (Router contains main controller)
 * 	also it provides services related to Routing, component routing/creating or main controller
 *
 * 		Singleton pattern - only one instance available by static get() function
 *
 *		Provides public Methods:
 *				public function loadPage($url)
 *		 				- main method - only index should call it
 *		   		debugVar(string $var, string $name)
 *		 				- saves Variable for debugging in 'Debug' pre-prepared component
 *		 		getBufferedData(?string $property = null)
 *						- returns array BufferedData or its one property from main controller. More about in Controller.php
 *		 		throwFatalError(string $message)
 *		 				- redirects to pre-prepared error site
 *
 *				public function getComponentController(string $ComponentName)
 *		 				- Returns false if Controller can't be loaded, null if there is no Controller required (from Components.json) for the component, or the Component
 *				public function getComponentView(string $ComponentName)
 *		 				- Returns false if View can't be loaded, null if there is no View required (from Components.json) for the component, or the View
 *
 *
 */

/*
 * 		Pre-made components and their basic styles:
 * 				Error
 * 						- main component - Error 404 - message through the URL - throwFatalError()
 * 				FlashMessage
 * 						- component that shows no JS popups - either popUps or flashErrors (only difference is background color)
 * 						  trough controllers $bufferedData
 * 				Debug
 * 						- fancy var_dumb - Controller(or Router)->debugVar($var, string $name)
 * 						- devModeEnabled config tells if is rendered
 *
*/

/*
	 Routes.json Expected Syntax:

		Mapping the URL:
			"_sub_path": {	'has to contain at least one "Part Name" or "_any"'	}
				- moving to the next part of the URL (f.e. www.web.com/ActivePart/SubPath/SubPath2/... to  www.web.com/PreviousPart/ActivePart/SubPath/)
			"Part text": {...}
				- Tells what to do, if Active part is "Part text". Optional if is "_any" set, but "Part text" has more importance if both is set
			"_any": {...}
				- Tells what to do, if Active part doesn't match any "Part text" or any "Part text" is not set.
					Optional if is  any "Part text" set

		Parameters:
			"_controller": "Controller name"
				- for setting main controller - Optional, but at least one has to be set
			"_view": "View name"
				- for setting main view
			"_parameter": "parameter name"
				- sets this part as parameter and saves it to $parameters array with "parameter name" key
					($parameters is what component controller handle() method is passed as parameter)
 */

/*
	Components.json Expected Syntax:
		Similar to Routes.json

		Mapping:
			"Component name": {...}
				- Tells what to do, if Active part is "Part text". Optional if is "_any" set, but "Part text" has more importance if both is set

		Parameters:
			"_controller": "Controller name"
				- for setting component controller - Optional
			"_view": "View name"
				- for setting component view
 */

class Router
{

	/**
	 * Variable where we store action (Default controller Method) if is
	 * @var string|null
	 */
	private ?string $action;

	/**
	 * Main page Controller
	 * @var Router|null
	 */
	private static ?Router $instance = null;

	/**
	 * Singleton instance
	 * @var AbstractController
	 */
	private AbstractController $pageController;

	/**
	 * Layout view
	 * @var string
	 */
	private string $pageView;

	/**
	 *  Array that is passed to every controller - contains parts from the URL that are set as parameter
	 * @var array
	 */
	private array $parameters;

	/**
	 * Routes.json  content
	 * @var object|mixed
	 */
	private object $routes;

	/**
	 *  Temp controller name from routes until parameters finish loading
	 * @var string|null
	 */
	private ?string $tempPageController;

	/**
	 * Temp view name from routes until parameters finish loading
	 * @var string|null
	 */
	private ?string $tempPageView;

	/**
	 * Constructor Loads Routes.json and Components.json (merges with preloaded DefaultComponents.json)
	 */
	private function __construct()
	{
		$this->routes = json_decode(FileReader::loadRouteFile(__DIR__ . '/../../Config/Routes.json'));
		$this->parameters = [];
		$this->action = "render";
	}

	/**
	 * Globally accessible method for putting Variable for debugging in debug component
	 * @param $var
	 * @param string $name
	 * @param bool $strict
	 */
	public function debugVar($var, string $name, bool $strict = false)
	{
		if ($strict) {
			echo $name . ": " . print_r($var) . "<br>";
			exit();
		} else
			$this->pageController->debugVar($var, $name);
	}

	/**
	 * Globally accessible method for getting BufferedData array (data that components threw "higher")
	 * @param string|null $property
	 * @return array
	 */
	public function getBufferedData(?string $property = null): array
	{

		if (is_null($property))
			$result = $this->pageController->getBufferedData();
		else
			$result = $this->pageController->getBufferedData()[$property];

		if ($result === null)
			$this->throwFatalError("Getting non existing Buffered data: $property", false, '505');

		return $result;
	}

	/**
	 * main method for handling the URL - renders page
	 * @param $url
	 */
	public function loadPage($url)
	{
		$parsedUrl = $this->parseURL($url[0]);                                        // URL parts array
		$indexURL = $this->parseURL(Config::getIni('indexURL'));    // Default URL from config
		$j = 0;
		for ($i = 0; $i < sizeof($indexURL); $i++) {        //deletes default route
			if ($indexURL[$i] == $parsedUrl[$i - $j]) {
				array_shift($parsedUrl);
				$j++;
			}
		}

		$route = $this->routes;
		$this->checkMainProprieties($route);                    // checks for controller and view and saves it if exist
		foreach ($parsedUrl as $subPart) {

			if (property_exists($route, "$subPart"))        // Checks if sub_path word at that url level exists in routing table
				$route = $route->$subPart;
			elseif (property_exists($route, "_any"))     // Or if _any exists in the routing table (specific words listed in the table have priority)
				$route = $route->_any;
			else $this->throwFatalError("Site does not exist!");

			$this->checkMainProprieties($route);
			if (property_exists($route, '_parameter')) {
				$this->parameters[$route->_parameter] = $subPart;
			}
			if (property_exists($route, '_action')) {
				$this->action = $subPart;
			}
		}

		ComponentFactory::get()->setGlobalParam($this->parameters);
		$this->setMainProprieties();
		if (isset($this->pageView) && !$this->pageController->hasView())
			$this->pageController->setView($this->pageView);
		$this->pageController->render();
	}

	/**
	 * Makes a fatal error. If strict mode is active it just echos an error, else it redirects on error site.
	 * Writes message only when dev mode is enabled
	 * @param string $message
	 * @param bool $safeRender
	 * @param string $code
	 */
	public function throwFatalError(string $message, bool $safeRender = false, string $code = '404')
	{
		if (class_exists('Error') && !$safeRender) {
			$sessionManager = new SessionManager();
			$sessionManager->set("ErrorMessage", $message);
			$sessionManager->set("ErrorCode", $code);
			header("Location: " . Config::getIni('indexURL') . "/Error");
			header("Connection: close");
			exit;
		}
		if (!Config::getIni("devModeEnabled"))
			$message = $code;
		echo "<h1>Error $code</h1><br><p>$message</p>";
		exit;
	}

	/**
	 * Checks for controller and view and saves it if exist
	 * @param object $route
	 */
	private function checkMainProprieties(object $route)
	{
		$this->tempPageController = ComponentFactory::get()->getControllerNameFromRoute($route);
		$this->tempPageView = ComponentFactory::get()->getViewPathFromRoute($route);
	}

	/**
	 *  Returns array of parameters cut from URL by slashes
	 * @param $url
	 * @return false|string[]
	 */
	private function parseURL($url)
	{
		// parsing URL to array
		$parsedURL = parse_url($url);
		// remove first slash
		$parsedURL["path"] = ltrim($parsedURL["path"], "/");
		// remove whitespaces
		$parsedURL["path"] = trim($parsedURL["path"]);
		// Remove last slash
		$parsedURL["path"] = preg_replace("/\/$/", "", $parsedURL["path"]);
		// Will return array of parameters cut from URL by slashes
		return explode("/", $parsedURL["path"]);
	}

	/**
	 * sets main proprieties of the page
	 */
	private function setMainProprieties()
	{
		if (is_null($this->tempPageController))
			$this->tempPageController = "Controllers\\BaseController";
		$controller = ComponentFactory::get()->getController($this->tempPageController);
		if (!is_null($controller)) {
			$controller->setAction($this->action);
			$this->pageController = $controller;
		} else
			$this->throwFatalError("Controller $this->tempPageController can't load", true);

		if (!is_null($this->tempPageView)) {
			$view = ComponentFactory::get()->getFullViewPath($this->tempPageView);
			if (!is_null($view))
				$this->pageView = $view;
			else
				$this->throwFatalError("View $this->tempPageView can't load", true);
		} else $this->pageView = __DIR__ . '/../Views/Layout.phtml';

	}

	/**
	 *  Returns Singleton instance
	 * @return Router
	 */
	public static function get(): Router
	{
		if (is_null(self::$instance))
			self::$instance = new Router();
		return self::$instance;
	}
}