<?php

namespace Core;

use Model\FileReader;

class ComponentFactory
{

	private object $componentRoutes;

	private array $globalParam;

	/**
	 * Components.json  content
	 * @var ComponentFactory|null
	 */
	private static ?ComponentFactory $instance = null;

	/**
	 */
	private function __construct()
	{
		$this->globalParam = [];
		$this->componentRoutes = json_decode(FileReader::loadRouteFile(__DIR__ . '/../../Config/Components.json'));
	}

	/**
	 * @param string $controllerName
	 * @param array $localParams
	 * @return AbstractController|null
	 */
	public function getController(string $controllerName, array $localParams = []): ?AbstractController
	{
		if (!class_exists($controllerName)) // if controller exists create it and pass it parameters
			return null;
		$controller = new $controllerName;
		if (!$controller instanceof AbstractController) {
			Router::get()->throwFatalError("Isn't a controller");
			return null;
		}
		if (($sessionManager = new SessionManager())->has("bufferedData")) {
			$controller->setBufferedData($sessionManager->get("bufferedData"));
			$sessionManager->remove("bufferedData");
		}
		$controller->handle(array_merge($this->globalParam, $localParams));
		return $controller;
	}

	/**
	 * @param object $input
	 * @return string|null
	 */
	public function getControllerNameFromRoute(object $input): ?string
	{
		if (!property_exists($input, '_controller'))
			return null;
		$pathArr = explode("\\", $input->_controller);
		$name = $pathArr[sizeof($pathArr) - 1];
		array_pop($pathArr);
		$path = join('\\', $pathArr);
		$controller = 'Controllers\\' . $path . '\\' . $name . 'Controller';
		if (class_exists($controller))
			return $controller;
		return null;
	}

	/**
	 * Returns Adequate Template if exists else null
	 * @param string $inputRoute
	 * @return string|null
	 */
	public function getFullViewPath(string $inputRoute): ?string
	{
		$pathArr = explode("\\", $inputRoute);

		$name = $pathArr[sizeof($pathArr) - 1];
		array_pop($pathArr);
		if (sizeof($pathArr) > 0)
			$route = join('/', $pathArr);
		else $route = "Components";
		$view = __DIR__ . "/../Views/{$route}/{$name}.phtml";
		if (file_exists($view))
			return $view;

		return null;
	}

	/**
	 * returns controller if exists or only view
	 * @param string $componentName
	 * @param array $localParams
	 * @return AbstractController|string|null
	 */
	public function getGlobalComponent(string $componentName, array $localParams)
	{
		if (!property_exists($this->componentRoutes, $componentName))
			return null;
		$componentRoute = $this->componentRoutes->$componentName;
		$viewName = $this->getViewPathFromRoute($componentRoute);
		if (sizeof(explode("\\", $viewName)) == 1)
			$viewName = "Components/$viewName";
		$view = $this->getFullViewPath($viewName);
		$controllerName = $this->getControllerNameFromRoute($componentRoute);
		if (is_null($view))
			return null;
		if (is_null($controllerName))
			return $view;
		if (class_exists($controllerName)) {
			$controller = new $controllerName;
			if (!$controller instanceof AbstractController)
				return null;
			$controller->setView($view);
			$controller->handle(array_merge($this->globalParam, $localParams));
			return $controller;
		}
		return null;
	}

	/**
	 * @param object $route
	 * @return string|null
	 */
	public function getViewPathFromRoute(object $route): ?string
	{
		if (!property_exists($route, "_view"))
			return null;
		return $route->_view;
	}

	/**
	 * @param array $globalParam
	 */
	public function setGlobalParam(array $globalParam): void
	{
		$this->globalParam = $globalParam;
	}

	/**
	 *  Throws fatal error if accessed before initializing
	 * @return ComponentFactory
	 */
	public static function get(): ComponentFactory
	{
		if (is_null(self::$instance))
			self::$instance = new ComponentFactory();
		return self::$instance;
	}
}