<?php

namespace Core;

use Model\FileReader;

/*
 *
 *  Class for getting any config variable
 *
 * 	For now only it does is provide variables from merge of 'Core/Default/DefaultConfig.ini' and 'Config/Config.ini'
 * 	'Config/Config.ini' has more importance if there's variable with same name
 *
 * 		Singleton pattern - only one instance available by static get() function
 *
 * 		Provides public function getIni(string $property)
 *
 *
 */

class Config
{

	/**
	 * @param string $code
	 * @return false|mixed
	 */
	public static function getErrorMessage(string $code)
	{
		$errorIni = parse_ini_string(self::loadConfig(__DIR__ . '/../../Config/ErrorMessages.ini'));
		if (isset($errorIni[$code]))
			return $errorIni[$code];
		return false;
	}

	/**
	 *  returns Config or false
	 * @param string $property
	 * @return false|mixed
	 */
	public static function getIni(string $property)
	{
		$ini = parse_ini_string(self::loadConfig(__DIR__ . '/../../Config/Config.ini')
		);
		if (isset($ini[$property]))
			return $ini[$property];
		return false;
	}

	/**
	 *    loads config or throws an error
	 * @param string $path
	 * @return string|void
	 */
	private static function loadConfig(string $path)
	{
		$conf = FileReader::loadFile($path);
		if (is_string($conf))
			return $conf;

		echo "Config didn't load";
		exit();
	}
}