<?php

namespace Core;

/*
 *
 * Abstract Parent of all Controllers.
 *
 *
 * 		Controller is a class that main purpose is to  contain, render and to handle data to its View
 * 		But also it handles components that are desired by its View (renderComponent($name)).
 *
 * 		Rendering:
 * 			- calls beforeRender();
 * 			- buffers rendering, so it can load and save desired components (Their controller or Views).
 * 			- asks these component controllers for $bufferedData and ads it to its own.
 * 			- asks these component controllers for $head and rewrites its own.
 * 			- handles these data.
 * 			- deletes previous render buffer.
 * 			- extracts array $data so View has available secured variables (keys from $data)
 * 			- extracts array $head so main View has available variables like title etc.
 * 			- renders now for real, but with already saved controllers (or only Views)
 *
 * 		abstract function handle($param) is determined to process parameters from URL provided by the router
 *
 * 		Every controller also provides public methods:
 * 			debugVar($var, string $name)
 * 					- Special case of BufferedData. It's determined for pre-made debug component - basically just fancy var_dump
 * 			renderComponent(string $componentName)
 * 					- used in the View - way how to render component (has to be set in Config/Components.json)
 * 			render()
 * 					- used by the Router to render main View and by its renderComponent() method
 * 			beforeRender()
 * 					- method that is called as last thing before rendering process begins
 * 			throwFlashError(string $message)
 * 					- flash error is just popup window, but the message/s are transferred trough $bufferedData array
 *
 * 			redirect($url)
 * 			setView($view)
 *			getBufferedData()
 * 			getHead()
 * 					- all speaking for itself
 *
 *
 * Code inspired by code from PHP MVC Tutorial from http://www.itnetwork.cz
 *
 */

abstract class AbstractController
{

	/**
	 *
	 * @var string
	 */
	protected string $action;

	/**
	 * Buffer for any stuff that Sub-controller (Controller of Component) throws
	 * @var array|array[]
	 */
	protected array $bufferedData = ["debugVariables" => [], "dangerAlerts" => [], "infoAlerts" => []];

	/**
	 * Data what will be visible in the Template
	 * @var array
	 */
	protected array $data;

	/**
	 * @var string[]
	 */
	protected array $head = ['title' => '', 'key_words' => '', 'description' => ''];

	/**
	 * Buffer for Sub-controllers or sub-views
	 * @var AbstractController[]|array
	 */
	private array $subComponents;

	/**
	 * Name of main view
	 * @var string
	 */
	protected string $view = "";

	/**
	 * @param string $action
	 */
	public function __construct(string $action = "render")
	{
		$this->action = $action;
		$this->subComponents = [];
		$this->data = [];
	}

	/**
	 *  renders component
	 * @param string $componentName
	 * @param string $componentAction
	 * @param array $componentParams
	 */
	public function component(string $componentName, string $componentAction = "render", array $componentParams = [])
	{
		//if is Controller/View ready & buffered, render it
		if (isset($this->subComponents[$componentName])) {
			//if it's controller, call its action method
			if ($this->subComponents[$componentName] instanceof AbstractController) {
				$componentAction = $this->subComponents[$componentName]->getAction();
				if (method_exists($this->subComponents[$componentName], $componentAction))
					$this->subComponents[$componentName]->$componentAction();
				else
					$this->subComponents[$componentName]->render();
			} else
				//else if no controller, then try render it as View
				$this->render($this->subComponents[$componentName]);

		} else {
			// Gets Controller and view, asks for bufferedData and head from controller and saves it
			$component = $this->getComponent($componentName, $componentParams);
			if ($component instanceof AbstractController) {
				$component->setAction($componentAction);
				$this->mergeBufferedData($component);
				$this->rewriteHead($component);
			}
			if (is_null($component))
				Router::get()->throwFatalError("Component $componentName didn't load");
			$this->subComponents[$componentName] = $component;

		}
	}

	/**
	 *  Puts an variable to debug
	 * @param $var
	 * @param string $name
	 */
	public function debugVar($var, string $name)
	{
		$this->bufferedData["debugVariables"][$name] = $var;
		(new SessionManager())->set("bufferedData", $this->bufferedData);
	}

	/**
	 * @return string
	 */
	public function getAction(): string
	{
		return $this->action;
	}

	/**
	 * @param string $action
	 * @return void
	 */
	public function setAction(string $action)
	{
		$this->action = $action;
	}

	/**
	 * @return array|array[]
	 */
	public function getBufferedData(): array
	{
		return $this->bufferedData;
	}

	/**
	 * @param array|array[] $bufferedData
	 */
	public function setBufferedData(array $bufferedData): void
	{
		$this->bufferedData = $bufferedData;
	}

	/**
	 * @return array|string[]
	 */
	public function getHead(): array
	{
		return $this->head;
	}

	/**
	 *  Method that processes URL
	 * @param array $param
	 * @return mixed
	 */
	abstract function handle(array $param);

	public function hasView(): bool
	{
		return $this->view !== "";
	}

	/**
	 *  Redirects to different page of the site
	 * @param string $url
	 */
	public function redirect(string $url)
	{
		header("Location: " . Config::getIni("indexURL") . "$url");
		header("Connection: close");
		exit;
	}

	/**
	 *    renders page/component
	 * @param string|null $componentView
	 */
	public function render(string $componentView = null)
	{
		if ($componentView != null) $toRenderView = $componentView;
		else
			$toRenderView = $this->view;
		$this->beforeRender();
		ob_start();                // To get $bufferedData and $subControllers
		$this->renderView($toRenderView);
		ob_end_clean();
		$this->renderView($toRenderView);    // To really render the View
	}

	/**
	 * @param string $view
	 */
	public function setView(string $view): void
	{
		$this->view = $view;
	}

	/**
	 *  pushes Danger Alert
	 * @param string $message
	 */
	public function alertError(string $message)
	{
		if (in_array($message, $this->bufferedData["dangerAlerts"]))
			return;
		$this->bufferedData["dangerAlerts"][] = $message;
		(new SessionManager())->set("bufferedData", $this->bufferedData);
	}

	/**
	 *  pushes Info Alert
	 * @param string $message
	 * @return void
	 */
	public function alertInfo(string $message)
	{
		if (in_array($message, $this->bufferedData["infoAlerts"]))
			return;
		$this->bufferedData["infoAlerts"][] = $message;
		(new SessionManager())->set("bufferedData", $this->bufferedData);
	}

	/**
	 * What to do before render
	 */
	protected function beforeRender()
	{
		$this->data["indexURL"] = Config::getIni('indexURL');
	}

	protected function getAsset($filename): string
	{

		$fileBaseName = pathinfo($filename, PATHINFO_FILENAME); // Just the file name
		$fileExt = pathinfo($filename, PATHINFO_EXTENSION); // Just the file extension
		// $assetType handles the asset folder inside the dist folder
		switch ($fileExt) {
			case 'js':
				$assetType = 'js'; // dir/scripts
				break;
			case 'png':
			case 'jpg':
			case 'gif':
				$assetType = 'images'; // dir/images
				break;
			case 'ico':
			case 'svg';
				$assetType = "icons";
				break;
			default:
				$assetType = 'styles'; // dir/styles
		}

		$handler = __DIR__ . "\..\..\www\dist\\$assetType\\";
		$externalHandler = "http://" . $_SERVER['SERVER_NAME'] . Config::getIni("indexURL") . "/www/dist/$assetType/"; // for viewing it. e.g: http://example.com/dist/styles/main.css

		$openHandler = opendir($handler);
		while ($file = readdir($openHandler)) {
			if ($file !== '.' && $file !== '..') {
				if (preg_match("/^" . $fileBaseName . "(.\w+)?." . $fileExt . "/i", $file, $name)) {
					return $externalHandler . $name[0];
				}
			}
		}
		closedir($openHandler);
		return '';
	}

	/**
	 * secures variable for HTML
	 * @param null $var
	 * @return array|mixed|string|null
	 */
	protected function secure($var = null)
	{
		if (!isset($var)) return null;
		elseif (is_string($var))
			return htmlspecialchars($var, ENT_QUOTES);
		elseif (is_array($var)) {
			foreach ($var as $key => $item) {
				$var[$key] = $this->secure($item);
			}
			return $var;
		} else
			return $var;
	}

	/**
	 *  Returns Controller or viewPath
	 * @param string $componentName
	 * @param array $localParams
	 * @return AbstractController|string
	 */
	private function getComponent(string $componentName, array $localParams = [])
	{
		$tempArr = explode('\\', get_class($this));

		//Pops out class name
		array_pop($tempArr);

		if (sizeof($tempArr) == 1)
			$localControllerFolder = join('\\', $tempArr) . '\\Components';
		else
			$localControllerFolder = join('\\', $tempArr);

		//shifts out Controllers\
		array_shift($tempArr);
		$localViewFolder = join('\\', $tempArr);
		if (sizeof($tempArr) > 0)
			$localViewFolder .= '\\';

		$componentController = ComponentFactory::get()->getController("\\$localControllerFolder\\$componentName" . 'Controller', $localParams);
		$componentView = ComponentFactory::get()->getFullViewPath("$localViewFolder$componentName");

		if (!is_null($componentController) && !is_null($componentView)) {
			$componentController->setView($componentView);
			return $componentController;
		}

		if (!is_null($componentView))
			return $componentView;

		if (is_null($componentController)) {
			$componentController = ComponentFactory::get()->getGlobalComponent($componentName, $localParams);
		}

		if (!is_null($componentController))
			return $componentController;

		Router::get()->throwFatalError("Component $componentName can't load", true);

		return "";
	}

	/**
	 *  Adds Buffered data from subController to its own
	 * @param AbstractController $controller
	 */
	private function mergeBufferedData(AbstractController $controller)
	{
		$array = $controller->getBufferedData();
		foreach ($array as $key => $data) {
			$this->bufferedData[$key] = array_merge($this->bufferedData[$key], $data);
		}

	}

	/**
	 * @param $view
	 */
	private function renderView($view)
	{
		if ($view) {
			extract($this->secure($this->data)); // Makes secured variables available in the template
			extract($this->data, EXTR_PREFIX_ALL, "");
			extract($this->head, EXTR_PREFIX_ALL, "");
			require($view);
		}
	}

	/**
	 * Rewrites it|s header with header of given Controller
	 * @param AbstractController $controller
	 */
	private function rewriteHead(AbstractController $controller)
	{
		$newHead = $controller->getHead();
		foreach ($newHead as $key => $data) {
			if ($data != "") {
				$this->head[$key] = $data;
			}
		}
	}
}