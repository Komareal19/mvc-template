<?php

namespace Core;

class SessionManager
{

	/**
	 *  Creates new session if not already created
	 * @param string|null $cacheExpire
	 * @param string|null $cacheLimiter
	 */
	public function __construct(?string $cacheExpire = null, ?string $cacheLimiter = null)
	{
		if (session_status() === PHP_SESSION_NONE) {

			if ($cacheLimiter !== null) {
				session_cache_limiter($cacheLimiter);
			}

			if ($cacheExpire !== null) {
				session_cache_expire($cacheExpire);
			}

			session_start();
		}
	}

	/**
	 * clears whole session
	 */
	public function clear(): void
	{
		session_unset();
	}

	/**
	 *    Returns value or null if key in session doesn't exist
	 * @param string $key
	 * @return mixed
	 */
	public function get(string $key)
	{
		if ($this->has($key)) {
			return $_SESSION[$key];
		}

		return null;
	}

	/**
	 * @param string $key
	 * @return bool
	 */
	public function has(string $key): bool
	{
		return array_key_exists($key, $_SESSION);
	}

	/**
	 *  Removes variable from session
	 * @param string $key
	 */
	public function remove(string $key): void
	{
		if ($this->has($key)) {
			unset($_SESSION[$key]);
		}
	}

	/**
	 *  Sets up a session variable
	 * @param string $key
	 * @param mixed $value
	 * @return SessionManager
	 */
	public function set(string $key, $value): SessionManager
	{
		$_SESSION[$key] = $value;
		return $this;
	}
}