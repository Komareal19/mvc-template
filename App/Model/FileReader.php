<?php

namespace Model;

class FileReader
{

	/**
	 *  Static class for reading Files or reading and decoding JSON files
	 * @param string $filepath
	 * @return false|string
	 */
	public static function loadFile(string $filepath)
	{
		if (file_exists($filepath)) {
			return file_get_contents($filepath);
		} else
			return false;
	}

	/**
	 *  loads routing file or throws an error
	 * @param string $path
	 * @return string|void
	 */
	public static function loadRouteFile(string $path)
	{
		$conf = self::loadFile($path);
		if (is_string($conf))
			return $conf;

		echo "Route File didn't load";
		exit;
	}

}