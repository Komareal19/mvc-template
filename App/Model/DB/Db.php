<?php

namespace Model\DB;

use Core\Config;
use Exception;
use PDO;

class Db
{

	private PDO $connection;

	public function __construct()
	{
		$conf = Config::getIni("DB");
		$this->connection = new PDO("mysql:host={$conf['host']}; port={$conf['port']}; dbname={$conf['name']};charset={$conf['charset']}",
			"{$conf['username']}",
			"{$conf['password']}",
			[
				PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
				PDO::MYSQL_ATTR_MULTI_STATEMENTS => false,
			]);
	}
	/*
	 *  PDO
	 * 		exec : int (non-Select, ideally wout params)
	 * 		query : PDOStatement (Select, ideally wout params)
	 * 		prepare : PDOStatement (Statement w params - select/non-select )
	 * 		lastInsertId : int
	 *
	 * 	PDOStatement
	 * 		bindParam(param, &value, type)
	 * 		valueParam(param, value, type)
	 * 		execute(?params)
	 * 		setFetchMode() defines, in what form will return one result
	 * 		fetch
	 * 		fetchAll
	 *
	 * 		FETCH_BOTH 	- array with string and int keys
	 * 		FETCH_NUM 	- array with  int keys
	 * 		FETCH_ASSOC - array with string keys, for same column name returns one value
	 * 		FETCH_NAMED - array with string keys, for same column name returns collections
	 * 		FETCH_OBJ 	- StdClass w public variables
	 * 		FETCH_CLASS - Some defined class
	 */

	/**
	 *  Insert
	 * @param string $sql
	 * @param DbParam[] $params
	 * @return int
	 * @throws Exception
	 */
	public function exec(string $sql, array $params = []): int
	{
		// Statement prep
		$stmt = $this->connection->prepare($sql);
		// Param pass
		foreach ($params as $param) {
			$stmt->bindValue($param->name, $param->value, $param->type);
		}
		// Statement exec
		if (!$stmt->execute())
			throw new Exception("Statement {$sql} didn't pass");
		return $stmt->rowCount();
	}

	/**
	 *    Return More rows
	 *
	 * @param string $sql
	 * @param string $className
	 * @param DbParam[] $params
	 * @return array
	 * @throws Exception
	 */
	public function getAll(string $sql, string $className, array $params = []): array
	{
		// Statement prep
		$stmt = $this->connection->prepare($sql);
		// Param pass
		foreach ($params as $param) {
			$stmt->bindValue($param->name, $param->value, $param->type);
		}
		// Statement exec
		if (!$stmt->execute())
			throw new Exception("Statement {$sql} didn't pass");
		$stmt->setFetchMode(PDO::FETCH_CLASS, $className);
		return $stmt->fetchAll();
	}

	/**
	 * Return One row
	 * @return int
	 */
	public function getLastId(): int
	{
		return $this->connection->lastInsertId();
	}

	/**
	 * Last ID
	 * @param string $sql
	 * @param string $className
	 * @param DbParam[] $params
	 * @return mixed
	 * @throws Exception
	 */
	public function getOne(string $sql, string $className, array $params = [])
	{
		// Statement prep
		$stmt = $this->connection->prepare($sql);
		// Param pass
		foreach ($params as $param) {
			$stmt->bindValue($param->name, $param->value, $param->type);
		}
		// Statement exec
		if (!$stmt->execute())
			throw new Exception("Statement {$sql} didn't pass");
		$stmt->setFetchMode(PDO::FETCH_CLASS, $className);
		return $stmt->fetch();
	}

	/**
	 * Return one value
	 * @param string $sql
	 * @param DbParam[] $params
	 * @return mixed
	 * @throws Exception
	 */
	public function getValue(string $sql, array $params = [])
	{
		// Statement prep
		$stmt = $this->connection->prepare($sql);
		// Param pass
		foreach ($params as $param) {
			$stmt->bindValue($param->name, $param->value, $param->type);
		}
		// Statement exec
		if (!$stmt->execute())
			throw new Exception("Statement {$sql} didn't pass");
		$stmt->setFetchMode(PDO::FETCH_NUM);
		return $stmt->fetch()[0];
	}

}