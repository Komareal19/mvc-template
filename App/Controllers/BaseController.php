<?php

namespace Controllers;

use Core\AbstractController;
use Core\Config;

class BaseController extends AbstractController
{

	/**
	 * @param array $param
	 * @return void
	 */
	function handle(array $param)
	{
		$this->data["mainComponent"] = "Debug"; //TODO: Either Delete or make main component
		if (isset($param["mainComponent"]))
			$this->data["mainComponent"] = $param["mainComponent"];
		$this->head = [
			'title' => '',
			'key_words' => '',
			'description' => '',
		];
	}

}