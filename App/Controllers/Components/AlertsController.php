<?php

namespace Controllers\Components;

use Core\AbstractController;
use Core\Router;

class AlertsController extends AbstractController
{

	/**
	 * @param array $param
	 * @return void
	 */
	public function handle(array $param)
	{
	}

	/**
	 * Prepares  popUps and Flash Errors
	 * @return void
	 */
	protected function beforeRender()
	{
		parent::beforeRender();
		$this->data["infoAlerts"] = Router::get()->getBufferedData("infoAlerts");
		$this->data["dangerAlerts"] = Router::get()->getBufferedData("dangerAlerts");

	}
}