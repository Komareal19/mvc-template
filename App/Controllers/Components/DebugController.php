<?php

namespace Controllers\Components;

use Core\AbstractController;
use Core\Config;
use Core\Router;

class DebugController extends AbstractController
{

	private bool $isDevMode;

	/**
	 * @param array $param
	 * @return void
	 */
	function handle(array $param)
	{
		$this->isDevMode = Config::getIni("devModeEnabled");

		// Developer mode is enabled message
		if ($this->isDevMode)
			$this->alertInfo("Developer mode is enabled");
	}

	public function render(string $componentView = null)
	{
		$this->data["debugVariables"] = Router::get()->getBufferedData("debugVariables");
		if (empty($this->bufferedData["debugVariables"]))                        // If no debugVariables do not render anything
			$this->isDevMode = false;
		if ($this->isDevMode) {
			parent::render($componentView);
		}
	}

}