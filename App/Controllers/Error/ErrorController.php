<?php

namespace Controllers\Error;

use Core\AbstractController;
use Core\Config;
use Core\SessionManager;

class ErrorController extends AbstractController
{

	/**
	 * @param array $param
	 * @return void
	 */
	function handle(array $param)
	{
		//Main handling
		$this->data["mainComponent"] = "error";
		$this->head = [
			'title' => 'Error',
			'key_words' => 'Error',
			'description' => 'Error',
		];

		//Handling URL Messages
		$message = "";
		$code = '500';
		$sessionManager = new SessionManager();
		if ($sessionManager->has("ErrorCode")) {
			$code = $sessionManager->get("ErrorCode");
			 $sessionManager->remove("ErrorCode");
		}
		if ($sessionManager->has("ErrorMessage")) {
			$message = $sessionManager->get("ErrorMessage");
			$sessionManager->remove("ErrorMessage");
		}
		if (!Config::getIni("devModeEnabled"))
			$message = Config::getErrorMessage($code);
		if (!is_string($message)) {
			$code = '500';
			$message = "ErrorMessage Wrong";
		}
		$message = $this->secure($message);
		$this->data["message"] = $message;
		$this->data["code"] = $code;
	}
}