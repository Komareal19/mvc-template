<?php
/*
 * Site that will get loaded, only thing that it does is
 * 	auto-loading all needed classes
 * 	and creating and calling Router - main class that controls whole site
 *	and passing it last part of the URL (Behind first /)
 *
 * 	New controllers/views styles should be in App/, Core/ is determined to have mostly common stuff for everyone
 *
 */
// TODO Popups
// TODO Update docs
require __DIR__ . '/../vendor/autoload.php';
$router = \Core\Router::get();
$router->loadPage([$_SERVER['REQUEST_URI']]);